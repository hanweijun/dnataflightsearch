# README #

This demo project is for Dnata technical accessment.

### Features: ###

This console application is created using .Net 5.0. In summary, it demonstrates the following features.

* 1. Dependency Injection. High level class depends on the abstraction (interface) of lower level class, so decouple the implementation, thus easy for unit testing.

* 2. Json configuration file (appsettings.json) stores application settings. 

* 3. SeriLog for logging. It shows log to console only in demo, but can be extended to other sinks like rolling files. In production, it will be useful to log to files for issue investigation.

* 4. 3-tier architechture. i.e. application layer for presentation, service layer for business logic, and reporsitory layer for data access.

* 5. Entity Framework as database access layer. It's assumed all fight data are stored in database, and uses EF to get data. A localCB file is created in Data folder for demo.

* 6. Error handling. Use Try-Catch block to handle the exceptions and log the error details using SeriLog.

* 7. Aynchronous methods to improve the performance and scalability.

* 8. Memory caching is used in service methods to improve the performance.

* 9. Unit test with MsTest and Moq
