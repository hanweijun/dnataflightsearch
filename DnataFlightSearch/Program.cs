﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using DnataLib.Services;
using DnataLib.Repositories;
using System.Threading.Tasks;
using System;
using Newtonsoft.Json;

namespace DnataFlightSearch
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var host = AppStartup();

            var dataService = ActivatorUtilities.GetServiceOrCreateInstance<IFlightSearchService>(host.Services);

            try
            {
                // call service method to get flights that meet a given rule.
                //var flights = await dataService.GetDepartbeforeCurrentDate();
                //var flights = await dataService.GetArrivalBeforeDeparture();
                //var flights = await dataService.GetByRuleOneAndTwo();
                var flights = await dataService.GetMoreThan2HourGroud();
                Console.WriteLine(JsonConvert.SerializeObject(flights));
            }
            catch (Exception ex)
            {
                // write exception details to log for investigation
                // in production, it's better to write to rolling files
                Log.Logger.Error(ex, "Unexpected error occurred.");
            }

            Console.WriteLine("Finished");
        }

        static void ConfigSetup(IConfigurationBuilder builder)
        {
            // add configuration file
            builder.SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                    .AddEnvironmentVariables();
        }

        static IHost AppStartup()
        {
            var builder = new ConfigurationBuilder();
            ConfigSetup(builder);

            // defining Serilog configs
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(builder.Build())
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .CreateLogger();

            // Initiated the denpendency injection container 
            var host = Host.CreateDefaultBuilder()
                        .ConfigureServices((context, services) => {
                            services.AddMemoryCache();
                            services.AddTransient<IFlightSearchService, FlightSearchService>();
                            services.AddTransient<IFlightRepository, FlightRepository>();
                        })
                        .UseSerilog()
                        .Build();

            return host;
        }
    }
}
