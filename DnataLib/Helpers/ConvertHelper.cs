﻿using DnataLib.Entity;
using DnataLib.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DnataLib.Helpers
{
    public static class ConvertHelper
    {
        public static IList<Flight> ConvertToFlightList(IList<FlightSegment> flightSegments)
        {
            var flights = new List<Flight>();
            foreach(var s in flightSegments)
            {
                var flight = flights.FirstOrDefault(f => f.FlightNo == s.FlightNo);
                if (flight!=null)
                {
                    // flight exists, add segment only
                    flight.Segments.Add(new Segment { DepartureDate = s.DepartureDate, ArrivalDate = s.ArrivalDate });
                }
                else
                {
                    // flight doesn't exist, add flight with segment
                    flight = new Flight
                    {
                        FlightNo = s.FlightNo,
                        Segments = new List<Segment> { new Segment { DepartureDate = s.DepartureDate, ArrivalDate = s.ArrivalDate } }
                    };
                    flights.Add(flight);
                }
            }

            return flights;
        }

        public static IList<FlightSegment> ConvertToFlightSegmentList(IList<Flight> flights)
        {
            var flightSegments = new List<FlightSegment>();
            foreach(var flight in flights)
            {
                foreach(var segment in flight.Segments)
                {
                    flightSegments.Add(new FlightSegment { 
                        Id=Guid.NewGuid().ToString(),
                        FlightNo= flight.FlightNo,
                        DepartureDate=segment.DepartureDate,
                        ArrivalDate=segment.ArrivalDate
                    });
                }
            }

            return flightSegments;
        }

    }
}
