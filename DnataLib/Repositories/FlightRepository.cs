﻿using DnataLib.Entity;
using DnataLib.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DnataLib.Repositories
{
    public class FlightRepository: IFlightRepository
    {
        private readonly ILogger<FlightSearchService> _log;
        private readonly DnataDbContext _db;

        public FlightRepository(ILogger<FlightSearchService> log, IConfiguration config)
        {
            _log = log;
            var connectionString = config.GetValue<string>("ConnectionStrings:DefaultConnection");
            _db = new DnataDbContext(connectionString);
        }

        // late binding version
        public IQueryable<FlightSegment> GetAllFlights()
        {
            _log.LogDebug("Geting all flights from database...");
            return _db.FlightSegments.OrderBy(f=>f.FlightNo).ThenBy(f=>f.DepartureDate).AsQueryable();
        }

        // early binding version
        public async Task<IList<FlightSegment>> GetAllFlightsAsync()
        {
            _log.LogDebug("Geting all flights from database...");
            return await _db.FlightSegments.OrderBy(f => f.FlightNo).ThenBy(f => f.DepartureDate).ToListAsync();
        }

        public async Task<FlightSegment[]> GetFlightsByFlightNosAsync(string[] flightNos)
        {
            _log.LogDebug("Geting flights by flight numbers from database...");
            var flightNosTemp = flightNos.Select(s => $"'{s}'").ToArray();
            var flightNoCsv = string.Join(",", flightNosTemp); 
            var sql = $"select * from FlightSegments where FlightNo in ({flightNoCsv})";
            return await _db.Set<FlightSegment>()
                .FromSqlRaw(sql).ToArrayAsync();
        }

        // This method is used to populate demo data into database table
        public async Task InsertIntoFlightSegments(FlightSegment[] flightSegments)
        {
            _db.FlightSegments.AddRange(flightSegments);
            await _db.SaveChangesAsync();
        }
    }
}
