﻿using DnataLib.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DnataLib.Repositories
{
    public interface IFlightRepository
    {
        IQueryable<FlightSegment> GetAllFlights();
        Task<IList<FlightSegment>> GetAllFlightsAsync();
        Task<FlightSegment[]> GetFlightsByFlightNosAsync(string[] flightNos);

        Task InsertIntoFlightSegments(FlightSegment[] flightSegments);
    }
}
