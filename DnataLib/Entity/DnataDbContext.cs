﻿using Microsoft.EntityFrameworkCore;

namespace DnataLib.Entity
{
    public class DnataDbContext : DbContext
    {
        private readonly string _conn;
        public DnataDbContext(string connectionString) : base()
        {
            _conn = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_conn);
        }

        public DbSet<FlightSegment> FlightSegments { get; set; }

    }
}
