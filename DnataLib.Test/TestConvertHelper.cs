﻿using DnataLib.Entity;
using DnataLib.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System;
using System.Linq;
using DnataLib.Model;

namespace DnataLib.Test
{
    [TestClass]
    public class TestConvertHelper
    {
        [TestMethod]
        public void TestConvertToFlightList()
        {
            var flightSegments = MakeTestFlightSegments();
            var flights = ConvertHelper.ConvertToFlightList(flightSegments);
            Assert.IsTrue(flights.Count==2 && flights.First(f=>f.FlightNo== "FL101").Segments.Count()==2);
        }

        private static IList<FlightSegment> MakeTestFlightSegments()
        {
            return new List<FlightSegment> {
                new FlightSegment{ FlightNo="FL100", DepartureDate=DateTime.Now, ArrivalDate=DateTime.Now },
                 new FlightSegment{ FlightNo="FL101", DepartureDate=DateTime.Now, ArrivalDate=DateTime.Now },
                new FlightSegment{ FlightNo="FL101", DepartureDate=DateTime.Now, ArrivalDate=DateTime.Now },
           };
        }

        [TestMethod]
        public void TestConvertToFlightSegmentList()
        {
            var flights = MakeTestFlights();
            var flightSegments = ConvertHelper.ConvertToFlightSegmentList(flights);
            Assert.IsTrue(flightSegments.Count == 3 && flightSegments.Count(f => f.FlightNo == "FL101") == 2);
        }
        private static IList<Flight> MakeTestFlights()
        {
            return new List<Flight> {
                new Flight{ FlightNo="FL100", Segments= new Segment[]{ new Segment { DepartureDate = DateTime.Now, ArrivalDate = DateTime.Now } }  },
                new Flight{ FlightNo="FL101", Segments= new Segment[]{ new Segment { DepartureDate = DateTime.Now, ArrivalDate = DateTime.Now }, new Segment { DepartureDate = DateTime.Now, ArrivalDate = DateTime.Now } }  },
           };
        }
    }
}
